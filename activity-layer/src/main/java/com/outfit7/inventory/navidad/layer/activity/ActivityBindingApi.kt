package com.outfit7.inventory.navidad.layer.activity

/**
 * Should replace O7Ads and inventory binding api
 */
// new "o7ads"
interface ActivityBindingApi {
    fun getInstanceId(): Int
}
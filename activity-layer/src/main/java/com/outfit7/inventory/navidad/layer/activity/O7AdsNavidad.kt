package com.outfit7.inventory.navidad.layer.activity

import com.outfit7.inventory.navidad.data.AppLayerPersistence

/**
 * ActivityBindingApi is a
 */
class O7AdsNavidad (val persistence: AppLayerPersistence) : ActivityBindingApi {

    override fun getInstanceId(): Int {
        persistence.incrementAndStore()
        return this.hashCode()
    }
}

package com.outfit7.inventory.navidad.layer.app

import android.app.Activity
import android.content.Context
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.outfit7.inventory.navidad.data.AppLayerPersistence
import com.outfit7.inventory.navidad.data.AppLayerPersistenceImpl
import com.outfit7.inventory.navidad.layer.activity.ActivityBindingApi
import com.outfit7.inventory.navidad.layer.activity.O7AdsNavidad

object AppLayer {

    private val o7adsMap = hashMapOf<Int, Pair<ActivityBindingApi, LifecycleObserver>>()
    private lateinit var persistence: AppLayerPersistence

    /**
     * znotraj init dagger component zgradi dependency tree za app layer
     */
    fun init(context: Context) {
        println("BLA: Do something useful")
        persistence =
            AppLayerPersistenceImpl(context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE))
    }

    // TODO: 14/03/2022 fix concurency
    // TODO: 14/03/2022 enforce same instance of activity and lifecycle
    fun getO7AdsInstance(activity: Activity, lifecycle: Lifecycle, isMainActivity: Boolean = false): ActivityBindingApi {
        if (!o7adsMap.containsKey(activity.hashCode())) {
            val lifecycleObserver = object: DefaultLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    println("BLA: Removing for activity ${owner.hashCode()}")
                    o7adsMap.remove(owner.hashCode())
                    super.onDestroy(owner)
                }
            }
            lifecycle.addObserver(lifecycleObserver)

            println("BLA: Number of buttons clicked ${persistence.load()}")
            o7adsMap.put(activity.hashCode(), Pair(O7AdsNavidad(persistence), lifecycleObserver)).also { println("BLA: Adding for activity ${activity.hashCode()}") }
        }
        return o7adsMap[activity.hashCode()]!!.first
    }

    fun getSize() = println("BLA: Size of map ${o7adsMap.size}")
}
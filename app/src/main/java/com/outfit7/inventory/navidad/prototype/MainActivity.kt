package com.outfit7.inventory.navidad.prototype

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.outfit7.inventory.navidad.layer.app.AppLayer

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        println("BLA: Main activity run")
        super.onCreate(savedInstanceState)

        findViewById<Button>(R.id.new_activity_layer_1).setOnClickListener {
            println("BLA: getting o7Ads -> ${AppLayer.getO7AdsInstance(this, this.lifecycle).getInstanceId()}")
            AppLayer.getSize()
        }

        findViewById<Button>(R.id.open_splash).setOnClickListener {
            startActivity(Intent(this, SplashActivity::class.java))
            println("BLA: Splash activity started")
        }
    }
}
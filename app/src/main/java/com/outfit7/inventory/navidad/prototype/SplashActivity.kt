package com.outfit7.inventory.navidad.prototype

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.outfit7.inventory.navidad.layer.app.AppLayer

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {

    override fun onCreate(savedInstanceState: Bundle?) {
        println("BLA: Main activity run")
        super.onCreate(savedInstanceState)

        findViewById<Button>(R.id.new_activity_layer_2).setOnClickListener {
            println("BLA: getting o7Ads -> ${AppLayer.getO7AdsInstance(this, this.lifecycle).getInstanceId()}")
            AppLayer.getSize()
        }

        findViewById<Button>(R.id.close_activity).setOnClickListener {
            println("Splash activity closed")
            finish()
        }
    }
}
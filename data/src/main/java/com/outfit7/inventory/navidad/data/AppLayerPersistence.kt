package com.outfit7.inventory.navidad.data

interface AppLayerPersistence {
    fun incrementAndStore()
    fun load(): Int
}
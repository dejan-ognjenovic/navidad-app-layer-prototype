package com.outfit7.inventory.navidad.data

import android.content.SharedPreferences

class AppLayerPersistenceImpl constructor(val sharedPreferences: SharedPreferences) : AppLayerPersistence {

    private val key = "DEKI"

    override fun incrementAndStore() {
        sharedPreferences.edit().putInt(key, load() + 1).apply()
    }

    override fun load(): Int {
        return sharedPreferences.getInt(key, 0)
    }
}